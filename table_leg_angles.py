 # CROSSED TABLE LEG CALCULATOR
 # This is a homebrewed calculator for finding:
 # - The required length of each piece of wood for the legs
 # - The angle to cut in to get the desired shape of the leg (halfway through)
 # - The displacement in mm if no angle cutter is available

 #  ---------                                       
 #  ---------         ``                            
 #     ///            /+-                    ``     
 #     ///              ./+-                ./+:     
 #     ///               ./+:`          `:+:.       
 #     ///                 `-+/.     `-//-`         
 #     /// Y (mm)             -+:``-/+-`            
 #     ///                    `-++++-     Alpha (deg)          
 #     ///                .-://:-`.-/+:.`           
 #     ///            `-/+:-`        `-/+:.         
 #  ---------       `/+:.   Beta (deg) `:+/-       
 #  ---------                              .`       
                                                  
                                                  
                                                  
                                                  
 #                	///       X (mm)	   ///  
 #                	//////////////////////////
 #                	///      			   ///	
 #                                         

# Load packages
import math
import re

# Define functions
def getIntegerInput(question):
	input_string = raw_input(question);
	return int(re.sub(r'\D', '', input_string));

# Input
width = getIntegerInput("Enter the width of the table legs (mm): ");
height = getIntegerInput("Enter the height of the table legs (mm): ");
leg_width = getIntegerInput("Width of the legs (mm): ");

# Calculate leg length with Pythagoras
leg_length = math.sqrt(width**2 + height**2);

# Use half the length for calculations
c = leg_length/2;

# Find angles between legs
cos_beta = ((2*c**2)-width**2)/(2*c*c);
beta = math.degrees(math.acos(cos_beta));
alpha = (360 - 2*beta)/2;

# Calculate cut angle (from 90 degrees)
B = abs(alpha-90);

# Calculate cut displacement on each side of leg
if(width != height):
	A = B-90;
	displacement = abs(leg_width/math.tan(math.radians(A)));
else:
	displacement = 0;

# Print results in console
print("\n\nRESULTS\n")
print("Leg length:\t\t %d \tmm" % leg_length);
print("Cut angle:\t\t %0.1f \tdeg\t(Deviation from 90 degrees)" % B);
print("Cut displacement:\t %d \tmm\t(Divide by two for distance from middle on each side of cut)" % displacement);
print("\n")

